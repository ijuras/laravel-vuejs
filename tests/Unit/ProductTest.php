<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Product;

class ProductTest extends TestCase
{
    public function testCreateProduct()
    {
        $response = $this->json('POST', '/api/products', ['name' => 'Sample Product', 'price' => 1.00]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'name' => 'Sample Product',
                'price' => 1.00
            ]);

        $this->assertDatabaseHas('products', [
            'name' => 'Sample Product',
            'price' => 1.00
        ]);
    }

    public function testUpdateProduct()
    {
        $product = factory(Product::class)->create();
        $response = $this->json('PUT', '/api/products/' . $product->id, ['name' => 'Updated Name']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'name' => 'Updated Name',
                'price' => $product->price,
            ]);

        $this->assertDatabaseHas('products', [
            'name' => 'Updated Name',
            'price' => $product->price
        ]);
    }
}
