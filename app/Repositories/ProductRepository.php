<?php

namespace App\Repositories;

use App\Product;

class ProductRepository
{
    public function index()
    {
        return Product::all();
    }

    public function store(array $params)
    {
        $product = new Product($params);
        $product->save();
        return $product;
    }

    public function update(Product $product, array $params)
    {
        $product->fill($params);
        $product->save();
        return $product;
    }
}
